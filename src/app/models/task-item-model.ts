import { TaskItemInterface } from '../interfaces/task-item-model-interface';

export class TaskItem implements TaskItemInterface {
  _id: string;
  _photo: string;
  _text: string;

  constructor(id: string, photo: string, text: string) {
    this._id = id;
    this._photo = photo;
    this._text = text;
  }

  id(): string {
    return this._id;
  }
  photo(): string {
    return this._photo;
  }
  text(): string {
    return this._text;
  }
}
