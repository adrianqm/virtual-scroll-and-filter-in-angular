import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { TaskItem } from '../../models/task-item-model';
import { TaskService } from '../../services/task.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})
export class TaskComponent implements OnInit, OnDestroy {
  taskItemList: TaskItem[] = [];
  searchText = '';
  taskSubscription = new Subscription();

  constructor(private taskSrv: TaskService) {}

  ngOnInit(): void {
    this.loadList();
  }

  loadList() {
    this.taskSubscription = this.taskSrv.taskItems().subscribe(
      (res) => (this.taskItemList = res)
    );
  }

  generateList() {
    this.taskSrv.createTaskItems();
  }

  ngOnDestroy(): void {
    this.taskSubscription.unsubscribe();
  }
}
