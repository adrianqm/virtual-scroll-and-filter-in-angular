import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { of } from 'rxjs';
import { TaskItem } from 'src/app/models/task-item-model';
import { TaskItemFilterPipe } from 'src/app/pipes/task-item-filter.pipe';
import { TaskService } from 'src/app/services/task.service';
import { TaskComponent } from './task.component';

describe('TaskComponent', () => {

  let fixture: ComponentFixture<TaskComponent>;
  let taskComponent: TaskComponent;
  let taskSrvSpy: jasmine.SpyObj<TaskService>;

  beforeEach(waitForAsync( () => {

    taskSrvSpy = jasmine.createSpyObj<TaskService>(
      'TaskService',['taskItems','createTaskItems']
    );

    taskSrvSpy.taskItems.and.callFake(()=>{
      return of([new TaskItem('1', 'photo.jpg', 'text')]);
    })

    TestBed.configureTestingModule({
      declarations: [
        TaskComponent,
        TaskItemFilterPipe
      ],
      providers: [
        { provide: TaskService, useValue: taskSrvSpy }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(waitForAsync( () => {
    fixture = TestBed.createComponent(TaskComponent);
    taskComponent = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create task component', () => {
    expect(taskComponent).toBeTruthy();
  });

  it('should get task items', () => {
    taskComponent.loadList();
    expect(taskSrvSpy.taskItems).toHaveBeenCalled();
  });

  it('should regenerate items list', () => {
    taskComponent.generateList();
    expect(taskSrvSpy.createTaskItems).toHaveBeenCalled();
  });

  it('check current load item', () => {
    expect(taskComponent.taskItemList.length).toBe(1);
    expect(taskComponent.taskItemList[0].id()).toBe('1');
    expect(taskComponent.taskItemList[0].photo()).toBe('photo.jpg');
    expect(taskComponent.taskItemList[0].text()).toBe('text');
  });
});
