import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TaskItemComponent } from './task-item.component';

describe('TaskItemComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        TaskItemComponent
      ],
    }).compileComponents();
  });

  it('should create task item component', () => {
    const fixture = TestBed.createComponent(TaskItemComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
