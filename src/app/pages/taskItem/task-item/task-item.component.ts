import { Component, Input } from '@angular/core';
import { TaskItem } from '../../../models/task-item-model';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss'],
})
export class TaskItemComponent {
  @Input() item: TaskItem;
  @Input() searchText: string;

  constructor() {
    this.item = new TaskItem('', '', '');
    this.searchText = '';
  }
}
