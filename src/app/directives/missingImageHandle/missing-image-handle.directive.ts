import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appMissingImageHandle]'
})
export class MissingImageHandleDirective {

  constructor(private el: ElementRef) {}

  @HostListener("error")
  private onError() {
    this.el.nativeElement.src = "/assets/img/NoImage.png";
  }
}
