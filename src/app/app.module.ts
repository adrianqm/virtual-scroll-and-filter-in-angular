import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaskComponent } from './pages/task/task.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { TaskItemFilterPipe } from './pipes/task-item-filter.pipe';
import { FormsModule } from '@angular/forms';
import { HighlightDirective } from './directives/highlight/highlight.directive';
import { MissingImageHandleDirective } from './directives/missingImageHandle/missing-image-handle.directive';
import { TaskItemComponent } from './pages/taskItem/task-item/task-item.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    TaskItemFilterPipe,
    HighlightDirective,
    MissingImageHandleDirective,
    TaskItemComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, ScrollingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
