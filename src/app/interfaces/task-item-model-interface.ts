export interface TaskItemInterface {
  _id: string;
  _photo: string;
  _text: string;
}
