import { TaskService } from './task.service';

describe('TaskService', () => {

  let service: TaskService;

  beforeEach(() => {
    service = new TaskService();
  });

  it('taskItems generated', () => {
    service.createTaskItems();
    service.taskItems().subscribe( items =>{
      expect(items.length).toBe(4000);
    });
  });
});
