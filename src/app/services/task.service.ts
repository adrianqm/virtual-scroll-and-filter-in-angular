import { Injectable } from '@angular/core';
import { TaskItem } from '../models/task-item-model';
import { BehaviorSubject } from 'rxjs';
import { loremIpsum } from "lorem-ipsum";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private _taskItems = new BehaviorSubject<TaskItem[]>([]);

  constructor()
  {
    this.createTaskItems();
  }

  taskItems() {
    return this._taskItems.asObservable();
  }

  createTaskItems()
  {
    let items:TaskItem[] = Array.from({length:4000})
      .map((_,i) => new TaskItem(i.toString(),"https://picsum.photos/id/"+i+"/500/500.jpg",loremIpsum()));
    this._taskItems.next(items);
  }
}
