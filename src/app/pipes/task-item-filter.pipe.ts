import { Pipe, PipeTransform } from '@angular/core';
import { TaskItem } from '../models/task-item-model';

@Pipe({
  name: 'taskItemFilter',
})
export class TaskItemFilterPipe implements PipeTransform {
  transform(items: TaskItem[], searchText: string): TaskItem[] {

    if (searchText == '') return items;

    searchText = searchText.toLocaleLowerCase();

    return items.filter((taskItem: TaskItem) => {
      let idValue = taskItem.id().toLocaleLowerCase().includes(searchText);
      let textValue = taskItem.text().toLocaleLowerCase().includes(searchText);
      return idValue || textValue;
    });
  }
}
