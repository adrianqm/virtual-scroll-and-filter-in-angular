import { TaskItem } from "../models/task-item-model";
import { TaskItemFilterPipe } from "./task-item-filter.pipe";


describe('TaskItemFilterPipe', () => {

  let pipe: TaskItemFilterPipe;

  let mockItems:TaskItem[] =[
    new TaskItem('1','photo1.jpg','text1'),
    new TaskItem('2','photo2.jpg','text2')
  ];

  beforeEach(() => {
    pipe = new TaskItemFilterPipe();
  });

  it('search with empty string', () => {
    expect(pipe.transform(mockItems,'').length).toEqual(2);
  });

  it('search one item with id equal to 2', () => {
    let items:TaskItem[] = pipe.transform(mockItems,'2');
    let itemFiltered = items[0];
    expect(items.length).toEqual(1);
    expect(itemFiltered.id()).toEqual('2');
  });

  it('search text that not exist', () => {
    let items:TaskItem[] = pipe.transform(mockItems,'3');
    expect(items.length).toEqual(0);
  });

});
