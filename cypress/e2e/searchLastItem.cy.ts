describe('SearchLastItem', () => {
  beforeEach(() => {
    cy.visit('/task');
  });

  it('get last array item by id', () => {
    cy.get('#search-text').type('3999');
    cy.get('#task-item-list').should('have.length', 1);
  });
});
