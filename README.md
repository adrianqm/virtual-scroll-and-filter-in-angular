## Virtual Scroll And Filter In Angular 15
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.1.1.

## Demo
![](./src/assets/demo.gif)

## Description
This a project using Angular 15.1.1 using CDK Virtual Scroll for large lists of items, with a search input that helps to filter the data in real time.
In this project you'll'find:

- A model for the items shown (with id, photo and random Lorem text).
- One directive for highlight the text searched and other to show a 'No image available' picture when the url proposed it's inaccessible.
- A pipe to compare the text with the id and the random text generated, and then filter it.
- A service to get the data generated locally.
- Unit testing (based in E2E) using Cypress. This test it's called 'searchLastItem.cy.ts and try to find the last item in the list that will be the item with id = 3999.
- Different view components (One for the list and other for the task item itself).


## Installation
Clone the project and use this command `npm i`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Usage
Use the serach input to filter the data shown based in the id and randomly generated text.

## Running cypress test

To open cypress run the command `ng run test_app:cypress-open` or `ng run test_app:cypress-run`. Then click the browser and select the test to run in the window (If you have run the first command).

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Support
adrianqm97@gmail.com


